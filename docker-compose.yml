version: '3.9'

services:
  minio:
    container_name: dev_minio
    image: minio/minio
    command: server --console-address ":9001" /data/
    ports:
      - "9000:9000"
      - "9001:9001"
    environment:
      - MINIO_ACCESS_KEY=${AWS_ACCESS_KEY_ID}
      - MINIO_SECRET_KEY=${AWS_SECRET_ACCESS_KEY}
    healthcheck:
      test:
        [
          "CMD",
          "curl",
          "-f",
          "http://localhost:9000/minio/health/live"
        ]
      interval: 30s
      timeout: 20s
      retries: 3
    networks:
      - minio_app
    volumes:
      - ./minio/:/data
    restart: "always"

  grafana:
    image: grafana/grafana
    container_name: grafana
    restart: unless-stopped
    environment:
      - GF_SERVER_ROOT_URL=http://my.grafana.server/
      - GF_INSTALL_PLUGINS=grafana-clock-panel
    ports:
      - '3000:3000'
    volumes:
      - 'grafana_storage:/var/lib/grafana'

  db:
    container_name: dev_pg_container
    image: postgres
    restart: always
    environment:
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
      POSTGRES_DB: ${POSTGRES_DB}
      PGDATA: /data/postgres
    volumes:
      - postgres:/data/postgres
    ports:
      - "5432:5432"
    networks:
      - postgres

  pgadmin:
    container_name: dev_pgadmin
    image: dpage/pgadmin4
    restart: always
    environment:
      PGADMIN_DEFAULT_EMAIL: admin@admin.com
      PGADMIN_DEFAULT_PASSWORD: root
    volumes:
      - my-data:/var/lib/pgadmin
    ports:
      - "5050:80"
    networks:
      - postgres

  mlflow:
    container_name: dev_mlflow_server
    restart: always
    build: ./mlflow
    image: mlflow_server
    ports:
      - "5000:5000"
    networks:
      - postgres
      - minio_app
    environment:
      - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
      - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
      - MLFLOW_S3_ENDPOINT_URL=${MLFLOW_S3_ENDPOINT_URL}
    command: mlflow server --backend-store-uri postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@db/${POSTGRES_DB} --default-artifact-root s3://${AWS_S3_BUCKET}/ --host 0.0.0.0

  prometheus:
    container_name: dev_prometheus
    image: prom/prometheus:latest
    restart: unless-stopped
    hostname: prometheus
    ports:
      - "9090:9090"
    volumes:
      - ./monitoring/prometheus.yml:/etc/prometheus/prometheus.yml
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'

volumes:
  grafana_storage:
  postgres:
  my-data:


networks:
  minio_app:
    driver: bridge
  postgres:
    driver: bridge
